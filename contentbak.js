// current URL (as it appears in the address bar in your browser)
var currentURL = window.location.href;

var classroom = "classroom.google.com";
if (currentURL.includes(classroom)) {
  insertCount();
}

function insertCount() {
  var waitForEl = function (selector, callback) {
    var el = document.getElementsByClassName(selector);
    var id_quickom = document.getElementById("quickom_link");
    if (el.length && !id_quickom) {
      callback();
    } else {
    setInterval(function () {
        waitForEl(selector, callback);
      }, 1000);
    }
  };

  var selector = "u0Snqb vraZ7e tLDEHd";
  waitForEl(selector, function () {
    var div = document.createElement("div");
    let title = "Meet Link";
    let href = "https://conference.quickom.com/dashboard/conference?id&page=1";
    let alink = "quickom.com";
    div.innerHTML = `<div id = "quickom_link" class="u0Snqb vraZ7e tLDEHd" guidedhelpid="streamCourseCode"><em class="VkMwfe">${title}</em><a jscontroller="kHXU4b" style="color: #FFFFFF" href="${href}">${alink}</a> <img style="margin-left: 5px" height="16" width="16" src="https://storage.googleapis.com/beowulf/media/Beowulf_Media_Kit/QUICKOM/QUICKOM_icon/QUICKOM_icon.jpg"></div>`
    var el = document.getElementsByClassName(selector);
    if (el[0]) el[0].after(div);
  });
}
